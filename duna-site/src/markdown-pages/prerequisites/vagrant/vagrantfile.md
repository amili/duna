---
slug: "/en/prerequisites/vagrantfile"
date: "2021-04-19"
title: "Vagrant file"
---

# Vagrantfile for the project

```
# -*- mode: ruby -*-
# vi: set ft=ruby :
box = "centos/7"

if Vagrant::VERSION == '1.8.5'
  ui = Vagrant::UI::Colored.new
  ui.error 'Unsupported Vagrant Version: 1.8.5'
  ui.error 'Version 1.8.5 introduced an SSH key permissions bug, please upgrade to version 1.8.6+'
  ui.error ''
end

Vagrant.configure("2") do |config|
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.ssh.insert_key = false
  config.vm.box_check_update = false
  config.vm.provider :virtualbox do |vb|
    config.vbguest.no_remote = true
    config.vbguest.auto_update = false
  end
  config.vm.define :master01 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.11"
    node.vm.network :forwarded_port, guest: 22, host: 24011, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.memory = 4096
    end
  end
  config.vm.define :master02 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.12"
    node.vm.network :forwarded_port, guest: 22, host: 24011, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.memory = 4096
    end
  end
config.vm.define :edge01 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.14"
    node.vm.network :forwarded_port, guest: 22, host: 24014, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.memory = 512
    end
  end
  config.vm.define :worker01 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.16"
    node.vm.network :forwarded_port, guest: 22, host: 24015, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.customize ["modifyvm", :id, "--memory", 2560]
      d.customize ["modifyvm", :id, "--cpus", 2]
      d.customize ["modifyvm", :id, "--ioapic", "on"]
    end
  end
  config.vm.define :worker02 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.17"
    node.vm.network :forwarded_port, guest: 22, host: 24016, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.customize ["modifyvm", :id, "--memory", 2560]
      d.customize ["modifyvm", :id, "--cpus", 2]
      d.customize ["modifyvm", :id, "--ioapic", "on"]
    end
  end
  config.vm.define :worker03 do |node|
    node.vm.box = box
    node.vm.network :private_network, ip: "10.10.10.18"
    node.vm.network :forwarded_port, guest: 22, host: 24016, auto_correct: true
    ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
    node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
    node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
    node.vm.provider "virtualbox" do |d|
      d.customize ["modifyvm", :id, "--memory", 2560]
      d.customize ["modifyvm", :id, "--cpus", 2]
      d.customize ["modifyvm", :id, "--ioapic", "on"]
    end
  end
end
```

