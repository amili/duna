---
slug: "/en/prerequisites/vagrant"
date: "2021-04-19"
title: "Vagrant"
---

## Installation

TODO

## Configuration

[Vagrant file](/en/prerequisites/vagrantfile)

## Start/stop machines

```
vagrant up
vagrant status
vagrant halt
```


## Links



