---
slug: "/en/ansible/config"
date: "2021-04-29"
title: "Ansible - configuration"
---

# Configuration in progress

**Version on 2021-05-01**

## Hosts

File `/etc/ansible/hosts`

```

[everybody]
master01.wcluster ansible_host=10.10.10.11 nodename=master01.wcluster.local
master02.wcluster ansible_host=10.10.10.12 nodename=master02.wcluster.local
edge01.wcluster ansible_host=10.10.10.14 nodename=edge01.wcluster.local
worker01.wcluster ansible_host=10.10.10.16 nodename=worker01.wcluster.local
worker02.wcluster ansible_host=10.10.10.17 nodename=worker02.wcluster.local
worker03.wcluster ansible_host=10.10.10.18 nodename=worker03.wcluster.local
ldap01.wcluster ansible_host=10.10.10.19 nodename=ldap01.wcluster.local

[clusterservers]
master01.wcluster ansible_host=10.10.10.11 nodename=master01.wcluster.local
master02.wcluster ansible_host=10.10.10.12 nodename=master02.wcluster.local
edge01.wcluster ansible_host=10.10.10.14 nodename=edge01.wcluster.local
worker01.wcluster ansible_host=10.10.10.16 nodename=worker01.wcluster.local
worker02.wcluster ansible_host=10.10.10.17 nodename=worker02.wcluster.local
worker03.wcluster ansible_host=10.10.10.18 nodename=worker03.wcluster.local

[ambariserver]
master01.wcluster ansible_host=10.10.10.11 nodename=master01.wcluster.local

[dbserver]
master01.wcluster ansible_host=10.10.10.11 nodename=master01.wcluster.local

[ldapserver]
ldap01.wcluster ansible_host=10.10.10.19 nodename=ldap01.wcluster.local
```

## Parameters

To avoid the check if host is in file `~/.ssh/known_hosts` while a playbook runs I add to the file `/etc/ansible/ansible.cfg` line:

```
[defaults]
host_key_checking = False
```

