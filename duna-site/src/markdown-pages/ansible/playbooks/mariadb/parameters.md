---
slug: "/en/ansible/playbooks/mariadb/parameters"
date: "2021-05-03"
title: "Parameters for MariaDB installing"
---

```
---
mysql_datadir: /var/lib/mysql
mysql_socket: /var/lib/mysql/mysql.sock
mysql_port: 3306
mysql_log_bin: /var/lib/mysql/mysql_binary_log
mysql_log: /var/log/mariadb/mariadb.log
mysql_pid_dir: /var/run/mariadb
mysql_pid_file: "{{ mysql_pid_dir }}/mariadb.pid"
mysql_innodb_log_buffer_size: 64M
mysql_innodb_buffer_pool_size: 1G
mysql_root_password: bigdata
ambari_password: [YOUR_PASSWORD]
ambari_database: ambari

# Supported database types: mysql,postgresql,...
database_type: mysql

databases:
  hive:
    name: 'hive'
    user: 'hive'
    pass: '[PASSWORD]'
    type: '{{ database_type }}'
  ranger:
    name: 'ranger'
    user: 'ranger'
    pass: '[PASSWORD]'
    type: '{{ database_type }}'
  rangerkms:
    name: 'rangerkms'
    user: 'rangerkms'
    pass: '[PASSWORD]'
    type: '{{ database_type }}'
  oozie:
    name: 'oozie'
    user: 'oozie'
    pass: '[PASSWORD]'
    type: '{{ database_type }}'
  superset:
    name: 'superset'
    user: 'superset'
    pass: '[PASSWORD]'
    encoding: 'utf8'
    type: '{{ database_type }}'
  druid:
    name: 'druid'
    user: 'druid'
    pass: '[PASSWORD]'
```

