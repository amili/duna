---
slug: "/en/ansible/playbooks/mariadb"
date: "2021-05-03"
title: "Installing MariaDB with Ambari"
---

There are several files:

- `mariadb.yml` - main script
- `mysql_secure_installation.yml` - script included into `mariadb.yml`. It removes test user, closes the possibility to log in as root except from localhost 
- `databases.yml` - script included in `mariadb.yml` - creates the databases
- `parameters.yml` - configuration file - parameters to use and databases to create
- `my.cnf.yml` - configuration file to place in `/etc/my.cnf` on database node


## Command

```
ansible-playbook -u root mariadb.yml
```

# Links 

- [Cloudera Playbook](https://github.com/cloudera/cloudera-playbook)
