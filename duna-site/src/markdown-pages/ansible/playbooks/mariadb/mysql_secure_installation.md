---
slug: "/en/ansible/playbooks/mariadb/secure"
date: "2021-05-03"
title: "MariaDB secure installation"
---

File `mysql_secure_installation.yml` is included in `mariadb.yml`.

```
---

- name: Create user ambari on localhost
  mysql_user:
    name: ambari
    password: "{{ ambari_password }}"
    priv: '*.*:ALL,GRANT'
    state: present
  ignore_errors: True

- name: Create user ambari
  mysql_user:
    name: ambari
    password: "{{ ambari_password }}"
    host: 'master01.wcluster'
    priv: '*.*:ALL,GRANT'
    state: present
  ignore_errors: True

- name: Set root password
  mysql_user:
    name: root
    password: "{{ mysql_root_password }}"
    state: present
  ignore_errors: True

- name: Remove anonymous users
  command: 'mysql -uroot -p{{ mysql_root_password }} -ne "{{ item }}"'
  with_items:
    - DELETE FROM mysql.user WHERE User=''

- name: Disable remote login for root
  command: 'mysql -uroot -p{{ mysql_root_password }} -ne "{{ item }}"'
  with_items:
    - DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('127.0.0.1', '::1', 'localhost')

- name: Remove the test database
  mysql_db:
    login_user: root
    login_password: "{{ mysql_root_password }}"
    db: test
    state: absent

- name: Reload privilege tables
  command: 'mysql -uroot -p{{ mysql_root_password }} -ne "{{ item }}"'
  with_items:
    - FLUSH PRIVILEGES

```
