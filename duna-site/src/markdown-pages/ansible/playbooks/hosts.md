---
slug: "/en/ansible/playbooks/hosts"
date: "2021-04-19"
title: "Hosts configuration and ssh-keys with Ansible"
---

Playbook file `hosts.yml`

This playbook uses the variable `nodename` which is set in [Ansible hosts configuration](en/ansible/hosts).

```
- hosts: everybody
  tasks:
  - name: Insert host descriptions to /etc/hosts
    blockinfile:
      path: /etc/hosts
      block: |
              10.10.10.11     master01.wcluster.local master01.wcluster master02
              10.10.10.12     master02.wcluster.local master02.wcluster master02
              10.10.10.14     edge01.wcluster.local edge01.wcluster edge01
              10.10.10.16     worker01.wcluster.local worker01.wcluster worker01
              10.10.10.17     worker02.wcluster.local worker02.wcluster worker02
              10.10.10.18     worker03.wcluster.local worker03.wcluster worker03
              10.10.10.19     ldap01.wcluster.local ldap01.wcluster ldap01
    tags: hosts
  - name: Insert name of host to /etc/hostname
    lineinfile:
      path: /etc/hostname
      regexp: ^(localhost.localdomain).*
      line: '{{ nodename }}'
      owner: root
      group: root
      mode: '0644'
    tags: hosts
  - name: Set hostname
    command: hostnamectl set-hostname {{ nodename }}
    tags: hosts
  - name: Add private keys
    copy:
      src: /home/amili/vagrant/share/id_rsa
      dest: /root/.ssh/id_rsa
      owner: root
      group: root
      mode: 0600
    tags: ssh_keys
```

# Links

- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [Ansible configuration](/en/ansible/config)
