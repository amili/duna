---
slug: "/en/ansible/playbooks/packages"
date: "2021-04-30"
title: "Update all packages and install some new with Ansible"
---

Playbook file `packages.yml`

```
- hosts: everybody
  tasks:
  - name: Upgrage all packages
    yum: name=* state=latest
    tags: packages
  - name: Install openssh-clients
    yum: 
      name: openssh-clients
      state: present
    tags: packages
  - name: Install curl
    yum: 
      name: curl
      state: present
    tags: packages
  - name: Install wget
    yum: 
      name: wget
      state: present
    tags: packages
  - name: Install unzip
    yum: 
      name: unzip
      state: present
    tags: packages
  - name: Install tar
    yum: 
      name: tar
      state: present
    tags: packages
  - name: Install python
    yum: 
      name: python.x86_64
      state: present
    tags: packages
  - name: Install gcc
    yum: 
      name: gcc
      state: present
    tags: packages
```

# Links

- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [Ansible configuration](/en/ansible/config)
