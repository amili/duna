---
slug: "/en/ansible/playbooks/selinux"
date: "2021-04-30"
title: "SELinux configuration with Ansible"
---

Playbook file `selinux.yml`

```
- hosts: everybody
  tasks :
  - name: Disable SELinux in config
    ansible.posix.selinux:
      state: disabled
    register: reboot_required
  - name: Rebooting
    shell: ( /bin/sleep 5 ; shutdown -r now "Ansible updates triggered" ) &
    async: 30
    poll: 0
    ignore_errors: true
    notify:
      - waiting for server to come back after reboot
    when: (ansible_distribution == 'CentOS' or ansible_distribution == 'Red Hat Enterprise Linux') and reboot_required.reboot_required == True
  - name: Check the umask
    command: umask
    register: umask_output
  - name: Print the umask
    debug:
      msg: "umask for {{ nodename }}: {{ umask_output.stdout }}"

  handlers:
  - name: waiting for server to come back after reboot
    local_action: wait_for host={{ nodename }} port=22 state=started delay=10 timeout=60
    become: no
```

# Links

- [ansible.posix.selinux – Change policy and state of SELinux](https://docs.ansible.com/ansible/latest/collections/ansible/posix/selinux_module.html)
- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [Ansible configuration](/en/ansible/config)
