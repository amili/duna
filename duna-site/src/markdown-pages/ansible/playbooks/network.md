---
slug: "/en/ansible/playbooks/network"
date: "2021-04-30"
title: "Network configuration with Ansible"
---

Playbook file `network.yml`

```
- hosts: clusterservers
  tasks :
  - name: Gather service facts
    service_facts:
  - name: Network configuration
    blockinfile:
      path: /etc/sysconfig/network
      block: |
              NETWORKING=yes
              HOSTNAME={{ nodename }}
      create: yes
      backup: yes
      owner: root
      group: root
      mode: 0644
    tags: networking
  - debug:
      msg: firewalld installed!
    when: "'firewalld.service' in services"
  - name: Stop firewall
    systemd:
      name: firewalld.service
      state: stopped
      enabled: no
```

# Links

- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [Ansible configuration](/en/ansible/config)
