---
slug: "/en/ansible/playbooks/ntp"
date: "2021-04-19"
title: "NTP configuration with Ansible"
---

Playbook file `ntp.yml`

```
- hosts: everybody
  tasks:
  - name: Set timezone to Europe/Paris
    community.general.timezone:
      name: Europe/Paris
  - name: Install NTP
    yum: name=ntp state=installed
    tags: ntp
  - name: Make sure NTP is started up
    service: name=ntpd state=running enabled=yes
    tags: ntp
```

# Links

- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [Ansible configuration](/en/ansible/config)
