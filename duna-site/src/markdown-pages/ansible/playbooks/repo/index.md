---
slug: "/en/ansible/playbooks/repo"
date: "2021-05-03"
title: "Ansible - local repo installation"
---

## Steps

### Step 1: Get repo

[Ansible playbook forgetting the repo](/en/ansible/playbooks/get_repo)

```
ansible-playbook -u root ansible/get_repo.yml
```



1. [Synchronize the repo directories](/en/ansible/playbooks/sync_repo)
1. [Fix a usage of the locally installed repo](/en/ansible/playbooks/fix_repo)


