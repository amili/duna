---
slug: "/en/ansible/playbooks/get_repo"
date: "2021-05-03"
title: "Ansible - prepare to local repo installation"
---

## Prepare the repos infrastructure

`get_repo.yml`


```
- hosts: ambariserver
  tasks:                                                                        
  - name: Upgrage all packages
    yum: name=* state=latest
    tags: packages
  - name: Install yum-utils
    yum: 
      name: yum-utils
      state: present
    tags: packages
  - name: Install createrepo
    yum: 
      name: createrepo
      state: present
    tags: packages
  - name: Install HTTP server
    yum: 
      name:  httpd
      state: present
    tags: packages
  - name: Start service httpd, if not started
    service:
      name: httpd
      state: started
  - name: Enable service httpd
    service:
      name: httpd
      enabled: yes
  - name: Create directory for Ambari repo
    file:
      path: /var/www/html/ambari/centos7
      state: directory
      owner: root
      group: root
      mode: 0775
      recurse: yes
  - name: Create directory for HDP repo
    file:
      path: /var/www/html/hdp/centos7
      state: directory
      owner: root
      group: root
      mode: 0775
      recurse: yes
  - name: Create directory for HDP-UTILS repo
    file:
      path: /var/www/html/hdp_utils/centos7
      state: directory
      owner: root
      group: root
      mode: 0775
      recurse: yes
  - name: Add ambari repository
    yum_repository:
      name: ambari-2.7.3.0
      description: Ambari yum repo
      file: ambari
      baseurl: https://external_ambari_repo_address/
      gpgcheck: no
  - name: Add HDP repository
    yum_repository:
      name: HDP-3.1.4.0
      description: HDP yum repo
      file: hdp
      baseurl: https://external_hdp_repo_address/
  - name: Add HDP-UTILS repository
    yum_repository:
      name: HDP-UTILS-1.1.0.22
      description: HDP-UTILS yum repo
      file: hdp_utils
      baseurl: https://external_hdp_utils_repo_address/
```

