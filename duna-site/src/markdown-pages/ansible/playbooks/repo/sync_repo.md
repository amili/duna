---
slug: "/en/ansible/playbooks/sync_repo"
date: "2021-05-03"
title: "Ansible - repos synchronization"
---

## Synchronization and creation of the repos

`sync_repo.yml`


```
- hosts: ambariserver
  tasks:
  - name: Ambari repo synchronization
    shell: reposync -r ambari -p /var/www/html/ambari/centos7/ambari-2.7.3.0
  - name: HDP repo synchronization
    shell: reposync -r hdp -p /var/www/html/hdp/centos7/HDP-3.1.4.0
  - name: HDP-UTILS repo synchronization
    shell: reposync -r hdp_utils -p /var/www/html/hdp_utils/centos7/HDP-UTILS-1.1.0.22

  - name: Create Ambari repo
    shell: createrepo /var/www/html/ambari/centos7/ambari-2.7.3.0
  - name: Create HDP repo
    shell: createrepo /var/www/html/hdp/centos7/HDP-3.1.4.0
  - name: Create HDP-UTILS repo
    shell: createrepo /var/www/html/hdp_utils/centos7/HDP-UTILS-1.1.0.22
```
