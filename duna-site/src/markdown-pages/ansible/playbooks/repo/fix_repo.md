---
slug: "/en/ansible/playbooks/fix_repo"
date: "2021-05-03"
title: "Ansible - switch to local repos"
---

## Replace external repos with the local ones

`fix_repo.yml`

```
- hosts: ambariserver
  tasks:
  - name: Remove Ambari repository (and clean up left-over metadata)
    yum_repository:
      name: ambari
      state: absent
    notify: yum-clean-metadata
  - name: Remove HDP repository (and clean up left-over metadata)
    yum_repository:
      name: hdp
      state: absent
    notify: yum-clean-metadata
  - name: Remove HDP-UTILS repository (and clean up left-over metadata)
    yum_repository:
      name: hdp_utils
      state: absent
    notify: yum-clean-metadata
  - name: Add local Ambari repository
    yum_repository:
      name: ambari
      description: Local ambari yum repo
      file: ambari
      baseurl: http://master01.wcluster.local/ambari/centos7/
  - name: Add HDP repository
    yum_repository:
      name: hdp
      description: HDP yum repo
      file: hdp
      baseurl: http://master01.wcluster.local/hdp/centos7/
  - name: Add HDP-UTILS repository
    yum_repository:
      name: hdp_utils
      description: HDP-UTILS yum repo
      file: hdp_utils
      baseurl: http://master01.wcluster.local/hdp_utils/centos7/
```
