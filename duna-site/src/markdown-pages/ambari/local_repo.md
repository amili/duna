---
slug: "/en/ambari/environment/repository"
date: "2021-04-29"
title: "Ambari - prepare environment - Set up local repository"
---

> Note: Here manual installation is described, for the installation with Ansible check [this page](/en/ansible/playbooks/repo)

[Task description](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/ch_using-local-repos.html)

Ambari host is `master01.wcluster.local`

From the parental machine:

```
ssh root@master01.wcluster.local
```

All steps below are made on `master01` node.

### Prerequisites

**Install yum utilities**

```
yum install yum-utils createrepo
```

**Install HTTP server**

I choose **Apache httpd** server to install.

```
yum update httpd
yum install httpd
systemctl start httpd
systemctl enable httpd
systemctl status httpd
```

To check the HTTP server open link [http://master01.wcluster:80](http://master01.wcluster:80)

The directory `/var/www/html/` exists and it is empty.


## Ambari repository

**Obtaining the repository**

```
wget -nv https://path/to/repos/ambari-2.7.3.0.repo -O /etc/yum.repos.d/ambari.repo
```

Let's have a look on the list of repos.

```
yum repolist
```

I check if the ambari repo is present.

```
repo id                           repo name                               status
ambari-2.7.3.0                    ambari Version - ambari-2.7.3.0             13
```
## HDP repositories

**Obtaining the repository**

```
wget -nv https://path/to/repos/hdp-3.1.4.0-315.repo -O /etc/yum.repos.d/hdp.repo
```

Let's have a look on the list of repos.

```
yum repolist
```

I check if the HDP repo is present.

```
repo id                           repo name                               status
HDP-3.1.4.0                       HDP Version - HDP-3.1.4.0                  201
HDP-GPL                           HDP-GPL                                      4
HDP-UTILS-1.1.0.22                HDP-UTILS Version - HDP-UTILS-1.1.0.22      16
```

# Links

- [Preparing to Set Up a Local Repository](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/getting_started_setting_up_a_local_repository.html)
- [Setting up a Local Repository with Temporary Internet Access](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/setting_up_a_local_repository_with_temporary_internet_access.html)
- [Fixing the bug with empty basepath](https://community.cloudera.com/t5/Community-Articles/ambari-2-7-3-Ambari-writes-Empty-baseurl-values-written-to/ta-p/249314)
