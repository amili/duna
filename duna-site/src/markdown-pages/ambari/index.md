---
slug: "/en/ambari"
date: "2021-04-19"
title: "Ambari my experiments"
---

1. [Prepare the environment](/en/ambari/environment)
1. [Install Ambary](/en/ambari/install)
1. [Install cluster with the wizard](/en/ambari/wizard)


# Links

- [Ambari start point](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/prepare_the_environment.html)
