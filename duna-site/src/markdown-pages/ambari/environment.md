---
slug: "/en/ambari/environment"
date: "2021-04-19"
title: "Prepare the environment for cluster"
---

# Intro

Before the start have a look on [prerequisites](/en/prerequisites)

# List of requerements

[Source](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/prepare_the_environment.html)

To do:

- Set Up Password-less SSH
- Set Up Service User Accounts
- Enable NTP on the Cluster and on the Browser Host
- Check DNS and NSCD
- Configuring iptables
- Disable SELinux and PackageKit and check the umask Value
- Download and set up database connectors
- Configuring a Database Instance for Ranger
- Install Databases for HDF services

# Set Up Password-less SSH

We provide access 
1. From parental machine to all VMs installed by Vagrant
2. Between all nodes 

## Create ssh-keys pair

TODO

**Requirements for permissions:**

- `.ssh` directory: 700 (drwx------)
- public key (`.pub` file): 644 (-rw-r--r--)
- private key (`id_rsa`): 600 (-rw-------)

## From parental machine to nodes

I do this with the configuration of Vagrant file:

```
ssh_pub_key = File.readlines("../.ssh/id_rsa.pub").first.strip
node.vm.provision 'shell', inline: "mkdir -p /root/.ssh"
node.vm.provision 'shell', inline: "echo #{ssh_pub_key} > /root/.ssh/authorized_keys"
```

## Between tne nodes

I use the same private ssh key for all nodes. Yes, it looks not so logical 

Ansible playbook is used to provide the connection between nodes.
[Ansible playbook](/en/ansible/playbooks/hosts)

```
  - name: Add private keys
    copy:
      src: /path/to/file/id_rsa
      dest: /root/.ssh/id_rsa
      owner: root
      group: root
      mode: 0600
    tags: ssh_keys
```

# Set Up Service User Accounts

Root account is used for installing.

## Download and set up database connectors

[MySQL](/en/ambari/environment/mysql)

## Set up local repository

[Set up local repository](/en/ambari/environment/local-repository)

# Check DNS and NSCD

## Edit the Host File

## Set the Hostname

## Edit the Network Configuration File

**What we need to do?**

Using a text editor, open the network configuration file on every host and set the desired network configuration for each host. For example:

```
vi /etc/sysconfig/network
```

Modify the `HOSTNAME` property to set the fully qualified domain name.

```
NETWORKING=yes
HOSTNAME=<fully.qualified.domain.name>
```

**How will we do this?**

Ansible playbook file `ansible/network.yml`

```
- hosts: clusterservers
  tasks:
  - name: Network configuration
    blockinfile:
      path: /etc/sysconfig/network
      block: |
              NETWORKING=yes
              HOSTNAME={{ nodename }}
      create: yes
      backup: yes
      owner: root
      group: root
      mode: 0644
    tags: networking
```

[Playbook file](/en/ansible/playbooks/network)

## Configuring iptables

**What?**

For Ambari to communicate during setup with the hosts it deploys to and manages, certain ports must be open and available. The easiest way to do this is to temporarily disable iptables, as follows:

```
systemctl disable firewalld
service firewalld stop
```

**How?**

We add new part to existing ansible playbook `network.yml`. 

```
- hosts: clusterservers
  tasks :
  - name: Gather service facts
    service_facts:
  - name: Network configuration
    blockinfile:
      path: /etc/sysconfig/network
      block: |
              NETWORKING=yes
              HOSTNAME={{ nodename }}
      create: yes
      backup: yes
      owner: root
      group: root
      mode: 0644
    tags: networking
  - debug:
      msg: firewalld installed!
    when: "'firewalld.service' in services"
  - name: Stop firewall
    systemd:
      name: firewalld.service
      state: stopped
      enabled: no
```

[Playbook file](/en/ansible/playbooks/network)

## Disable SELinux and PackageKit and check the umask Value

**What?**

- disable SELinux
- since I have no PackageKit installed I skip this step
- UMASK. I do not change it, only check if everything is ok

**How**

Ansible playbook `ansible/selinux.yml`

```
- hosts: clusterservers
  tasks :
  - name: Disable SELinux in config
    lineinfile:
      path: /etc/selinux/config
      regexp: '^SELINUX='
      line: 'SELINUX=enforcing'
    tags: selinux
  - name: Stop SELinux now
    command: setenforce 0
    tags: selinux
  - name: Check the umask
    command: umask
    register: umask_output
  - name: Print the umask
    debug:
      msg: "umask for {{ nodename }}: {{ umask_output.stdout }}"
```

[Playbook file](/en/ansible/playbooks/selinux)

## Download and set up database connectors

# Links

- [Ambari prepare environments](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/prepare_the_environment.html)

