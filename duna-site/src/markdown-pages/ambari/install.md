---
slug: "/en/ambari/install"
date: "2021-04-30"
title: "Ambari installation"
---

Before installation

1. [Prerequisites](/en/prerequisites)
1. [Prepare the environment](/en/ambari/environment)

# What?

[Task description](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/install-ambari-server-rhel7.html)

Ambari host is `master01.wcluster.local`

From the parental machine:

```
ssh root@master01.wcluster.local
```

All steps below are made on `master01` node.

```
yum install ambari-server
```

# Links

- [Ambari start point](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/prepare_the_environment.html)
