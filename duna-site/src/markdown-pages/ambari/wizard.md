---
slug: "/en/ambari/wizard"
date: "2021-04-30"
title: "Ambari wizard"
---

## Before

1. [Prepare the environment](/en/ambari/environment)
1. [Install Ambary](/en/ambari/install)

## Start Ambari server

```
[master01]# ambari-server start
Using python  /usr/bin/python
Starting ambari-server
Ambari Server running with administrator privileges.
Organizing resource files at /var/lib/ambari-server/resources...
Ambari database consistency check started...
Server PID at: /var/run/ambari-server/ambari-server.pid
Server out at: /var/log/ambari-server/ambari-server.out
Server log at: /var/log/ambari-server/ambari-server.log
Waiting for server start..............................
Server started listening on 8080

DB configs consistency check: no errors and warnings were found.
Ambari Server 'start' completed successfully.
```
Open the page `http://master01.wcluster.local:8080` and click **Launch**

## 0 - Get started

Name your cluster: wcluster

## 1 - Select Version

HDP-3.1

Use Local Repository

| OS | Name | Base URL |
|----|------|----------|
| redhat7 | HDP-3.1 | http://master01.wcluster.local/hdp/centos7/HDP-3.1.4.0/ |
| redhat7 | HDP-UTILS-1.1.0.22 | http://master01.wcluster.local/hdp/centos7/HDP-UTILS-1.1.0.22/ |

## 2 - Install Options

**Target Hosts**

```
master01.wcluster.local
master02.wcluster.local
edge01.wcluster.local
worker01.wcluster.local
worker02.wcluster.local
worker03.wcluster.local
```

**Host Registration Information**

Choose "Provide your SSH Private Key to automatically register hosts" and select your file with rsa-key.

SSH User Account: root
SSH Port Number: 22


## 3 - Confirm Hosts

Check the status for all hosts.

## 4 - Choose Services

**Choose File System**  

| Service | Version | Description |
|---------|---------|-------------|
| HDFS | 3.1.1.3.1 | Apache Hadoop Distributed File System |

**Choose services**

| Service | Version | Description |
|---------|---------|-------------|
| YARN + MapReduce2 | 3.1.0 | Apache Hadoop NextGen MapReduce (YARN) |
| Tez	|0.9.0.3.1 | Tez is the next generation Hadoop Query Processing framework written on top of YARN. |
| Hive	|3.0.0.3.1 | Data warehouse system for ad-hoc queries & analysis of large datasets and table & storage management service |
| HBase	|2.0.0.3.1 | Non-relational distributed database and centralized service for configuration management & synchronization |
| Pig	|0.16.1.3.1 | Scripting platform for analyzing large datasets |
| Sqoop	|1.4.7	| Tool for transferring bulk data between Apache Hadoop and structured data stores such as relational databases |
| Oozie	|4.4.0	| System for workflow coordination and execution of Apache Hadoop jobs. This also includes the installation of the optional Oozie Web Console which relies on and will install the ExtJS Library. |
| ZooKeeper|	3.4.9.3.1 | Centralized service which provides highly reliable distributed coordination |
| Storm	| 1.2.1 | Apache Hadoop Stream processing framework |
| Accumulo |	1.7.0 |  Robust, scalable, high performance distributed key/value store. |
| Infra Solr |	0.1.0 | Core shared service used by Ambari managed components. |
| Ambari Metrics | 0.1.0 | A system for metrics collection that provides storage and retrieval capability for metrics collected from the cluster |
| Atlas	| 0.7.0.3.1 | Atlas Metadata and Governance platform |
| Kafka | 1.0.0.3.1 | A high-throughput distributed messaging system |
| Knox	| 0.5.0.3.1 | Provides a single point of authentication and access for Apache Hadoop services in a cluster |
| Log Search |	0.5.0	| Log aggregation, analysis, and visualization for Ambari managed services. This service is Technical Preview. |
| Ranger|	1.2.0.3.1 |	Comprehensive security for Hadoop |
| Ranger KMS | 1.2.0.3.1 | Key Management Server |
| SmartSense | 1.5.1.2.7.3.0-139 | SmartSense - Hortonworks SmartSense Tool (HST) helps quickly gather configuration, metrics, logs from common HDP services that aids to quickly troubleshoot support cases and receive cluster-specific recommendations. |
| Spark2 | 2.3.0 | Apache Spark 2.3 is a fast and general engine for large-scale data processing. |
| Zeppelin Notebook | 0.8.0 | A web-based notebook that enables interactive data analytics. It enables you to make beautiful data-driven, interactive and collaborative documents with SQL, Scala and more. |
| Druid | 0.12.1 | A fast column-oriented distributed data store. |
| Superset | 0.23.0 | Superset is a data exploration platform designed to be visual, intuitive and interactive. This service is Technical Preview. |

## 5 - Assign Masters

- SNameNode: 
- NameNode:
- Timeline Service V1.5:
- ResourceManager:
- Timeline Service V2.0 Reader:
- YARN Registry DNS:
- History Server:
- Hive Metastore:
- HiveServer2:
- HBase Master:
- Oozie Server:
- ZooKeeper Server:
- ZooKeeper Server:
- ZooKeeper Server:
- Nimbus:
- Storm UI Server:
- DRPC Server:
- Accumulo Tracer:
- Accumulo Master:
- Accumulo Monitor:
- Accumulo GC:
- Infra Solr Instance:
- Metrics Collector:
- Grafana:
- Atlas Metadata Server:
- Kafka Broker:
- Knox Gateway:
- Log Search Server:
- Ranger Usersync:
- Ranger Admin:
- Ranger KMS Server:
- Activity Explorer:
- HST Server:
- Activity Analyzer:
- Spark2 History Server:
- Zeppelin Notebook:
- Druid Overlord:
- Druid Coordinator:
- Druid Broker:
- Superset:
- Druid Router:

## 6 -Assign Slaves and Clients

Assign Slaves and Clients

I left everything by default

## 7 - Customize Services - Credentials

| Service | Username | Password |
|---------|----------|----------|
| Accumulo Root	| N/A | |	
| Accumulo Instance Secret | N/A | |
| Grafana Admin	| admin | |
| Atlas Admin User | admin | |
| Druid Metadata User | druid | |
| Hive Database	| hive | |
| Knox Master Secret | N/A | |	
| Log Search | ambari_logsearch_admin |
| Oozie Database | oozie | |
| Ranger Admin User Credentials | admin | |
| Ranger Admin Credentials For Ambari User | amb_ranger_admin | Created automatically |
| Ranger Admin DB Credentials | rangeradmin | |
| Ranger Usersync User's Password | N/A | |
| Ranger Tagsync User's Password | N/A | |
| Ranger KMS Keyadmin User's Password | N/A | |	
| Ranger KMS Master Key Password | N/A | |	
| Ranger KMS DB Credentials | rangerkms | |
| Activity Explorer's Admin | N/A | |	
| Superset Database | superset | |
| Superset Secret | N/A | |	
| Superset Admin | admin | |


## 7 - Customize Services - Databases

**DRUID META DATA STORAGE**

- Druid Metadata storage database name: druid
- Druid Metadata storage type: DERBY
- Metadata storage user: druid
- Metadata storage password: * * * 
- Metadata storage hostname: localhost
- Metadata storage port: 1527
- Metadata storage connector url: jdbc:derby://localhost:1527/druid;create=true

**HIVE**

- Hive Database: New MySQL
- Database Name: hive
- Database Username: hive
- Database URL: jdbc:mysql://master02.wcluster.local/hive?createDatabaseIfNotExist=true
- Hive Database Type: mysql
- JDBC Driver Class: com.mysql.jdbc.Driver
- Database Password: * * *

**OOZIE**

- Oozie Database: New Derby
- Database Name: oozie
- Database Username: oozie
- Database URL: jdbc:derby:${oozie.data.dir}/${oozie.db.schema.name}-db;create=true
- JDBC Driver Class: org.apache.derby.jdbc.EmbeddedDriver


**RANGER**

- DB FLAVOR: MYSQL
- Ranger DB name: ranger
- Ranger DB username: rangeradmin
- JDBC connect string for a Ranger database: jdbc:mysql://:3306/ranger
- Ranger DB host: **master01.wcluster.local**
- Driver class name for a JDBC Ranger database: com.mysql.jdbc.Driver
- Ranger DB password: * * *

Setup Database and Database User: **Yes**

- Database Administrator (DBA) username: root
- Database Administrator (DBA) password: **[Password for root on MariaDB]**
- JDBC connect string for root user: jdbc:mysql://master01.wcluster.local:3306

And press "Test connection"

> Note: If connection is failed try the commands

```
[master01]# mysql -u root -p
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '[Password for root on MariaDB]' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'master01.wcluster.local' IDENTIFIED BY '[Password for root on MariaDB]' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

**Ranger KMS**

Analogically

**Superset**

- Superset Database name: superset
- Superset Database type: SQLITE
- Superset Database user: superset
- Superset Database password: * * *
- Database hostname: localhost


## 7 - Customize services - Directories


## 7 - Customize services - Usernames



 DataNode maximum Java heap size: 967

Advanced druid-env -> druid.historical.jvm.heap.memory = 1024 MB

## 8 - Review

Save the configuration if you want and press "Deploy".


## 9 - Install, Start and Test

Good luck!

# Links

- [Ambari start point](https://docs.cloudera.com/HDPDocuments/Ambari-2.7.1.0/bk_ambari-installation/content/prepare_the_environment.html)
