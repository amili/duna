---
slug: "/en/quickstart"
date: "2021-04-30"
title: "Quick restart"
---

## Vagrant start all nodes

More information about vagrant config [here](/en/prerequisites/vagrant)

```
vagrant up
```

## Ansible for preparing the nodes

For the moment there are several ansible playbooks, it is possible that they will be unied later.

[More about Ansible commands](en/cheatsheets/ansible).

[More about Ansible configuration](en/ansible/config).

```
ansible-playbook -u root ansible/hosts.yml
ansible-playbook -u root ansible/ntp.yml
ansible-playbook -u root ansible/network.yml
ansible-playbook -u root ansible/selinux.yml
ansible-playbook -u root ansible/packages.yml
```

- [hosts.yml](/en/ansible/playbooks/hosts)
- [ntp.yml](/en/ansible/playbooks/ntp)
- [network.yml](/en/ansible/playbooks/network)
- [selinux.yml](/en/ansible/playbooks/selinux)
- [packages.yml](/en/ansible/playbooks/packages)

*Note:* to check manually if SELinux is disabled use the command:

```
sestatus
```

Status should be `disabled`!

## Local repositories - STEP 1

There are two variants for local repositories:

1. [Installing manually - step 1](/en/ambari/environment/repository)
1. [Ansible playbook forgetting the repo](/en/ansible/playbooks/get_repo)

```
ansible-playbook -u root ansible/get_repo.yml
```




## Install ambari-server

[Install ambari-server page](/en/ambari/install)

From the parental machine:

```
ssh root@master01.wcluster.local
```

All steps below are made on `master01` node.

```
yum install ambari-server -y
```

## Install connector

```
yum install mysql-connector-java -y
ambari-server setup --jdbc-db=mysql --jdbc-driver=/usr/share/java/mysql-connector-java.jar
```

Server response example:

```
Using python  /usr/bin/python
Setup ambari-server
Copying /usr/share/java/mysql-connector-java.jar to /var/lib/ambari-server/resources/mysql-connector-java.jar
If you are updating existing jdbc driver jar for mysql with mysql-connector-java.jar. Please remove the old driver jar, from all hosts. Restarting services that need the driver, will automatically copy the new jar to the hosts.
JDBC driver was successfully initialized.
Ambari Server 'setup' completed successfully.
```

## Check the DDL

We check manually if there is a file `Ambari-DDL-MySQL-CREATE.sql` among the libraries:

```
ls /var/lib/ambari-server/resources/
```

## Install MariaDB

- [Installing MariaDB manually](/en/ambari/environment/mysql)
- [Installing MariaDB with Ansible playbook](/en/ansible/playbooks/mariadb)

```
ansible-playbook -u root ansible/mariadb/mariadb.yml
```

*Note:* User and Database `ambari` were created manually - FIXME

```
mysql -u root -p
```

```
CREATE USER 'ambari'@'%' IDENTIFIED BY 'bigdata';
GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'%';
CREATE USER 'ambari'@'localhost' IDENTIFIED BY 'bigdata';
GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'localhost';
CREATE USER 'ambari'@'master01.wcluster.local' IDENTIFIED BY 'bigdata';
GRANT ALL PRIVILEGES ON *.* TO 'ambari'@'master01.wcluster.local';
FLUSH PRIVILEGES;
```

```
CREATE USER 'ranger'@'%' IDENTIFIED BY 'ranger_bigdata8';
GRANT ALL PRIVILEGES ON *.* TO 'ranger'@'%';
CREATE USER 'ranger'@'localhost' IDENTIFIED BY 'ranger_bigdata8';
GRANT ALL PRIVILEGES ON *.* TO 'ranger'@'localhost';
CREATE USER 'ranger'@'master01.wcluster.local' IDENTIFIED BY 'ranger_bigdata8';
GRANT ALL PRIVILEGES ON *.* TO 'ranger'@'master01.wcluster.local';
CREATE USER 'ranger'@'master01' IDENTIFIED BY 'ranger_bigdata8';
GRANT ALL PRIVILEGES ON *.* TO 'ranger'@'master01';
FLUSH PRIVILEGES;



GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'bigdata' WITH GRANT OPTION;

FLUSH PRIVILEGES;
```


```
mysql -uambari -p
CREATE DATABASE ambari;
USE ambari;
SOURCE /var/lib/ambari-server/resources/Ambari-DDL-MySQL-CREATE.sql;
```




## Local repositories - STEP 2

There are two variants for local repositories:

- [Installing manually](/en/ambari/environment/repository2)
- [With Ansible playbook](/en/ansible/playbooks/repo)

```
ansible-playbook -u root ansible/sync_repo.yml
ansible-playbook -u root ansible/fix_repo.yml
```


> Note: To fix bug with empty baseurl in Ambari have a look on the last section in "Installing manually"


## Next step

[Ambari server setup](/en/ambari/setup)



# Links

- [Ansible cheatsheet](/en/cheatsheets/ansible)
- [How to disable SELinux](https://linuxize.com/post/how-to-disable-selinux-on-centos-7/)

