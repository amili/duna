---
slug: "/en/cheatsheets/ansible"
date: "2021-04-19"
title: "Ansible basics for me"
---

# Installation

TODO

# Configuration

## Inventory

File `/etc/ansible/hosts`

[My configuration](/en/ansible/hosts)


## Check configuration

We will use special package to check ansible configuration without performing the commands. It is [ansible-lint](https://ansible-lint.readthedocs.io/en/latest/)

```
sudo apt install ansible-lint
```

```
ansible-lint playbook.yml
```

# Work

## Basic Commands

Check if nodes response:

```
ansible all -u root -m ping
```

Send a small command to everybody:

```
ansible all -u root -a "/bin/echo hello"
```

## Launch the playbook

```
ainsible-playbook -u root ntp.yml
```



## Links

- [Ansible](https://www.ansible.com/resources/get-started)
- [Ansible-lint](https://ansible-lint.readthedocs.io/en/latest/)
- [Ansible commands](http://willthames.github.io/2016/09/21/using-command-and-shell-in-ansible.html)
- [Yum module in Ansible](https://docs.ansible.com/ansible/2.3/yum_module.html)
- [Ansible Ad-hoc commands](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html#intro-adhoc)
- [Copy files with ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html#copy-module)

