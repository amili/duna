---
slug: "/en/cheatsheets/openvpn"
date: "2021-04-19"
title: "OpenVPN Client on Ubuntu"
---

## Installation

See [Tutorial](https://openvpn.net/vpn-server-resources/connecting-to-access-server-with-linux/)

## Create Connection

If the configuration file is `/path/to/file/conname.ovpn`, then you could use this command to create a new connection:

```
sudo nmcli connection import type openvpn file /path/to/file/conname.ovpn
```

## Use Connection

**Activate connection**

```
nmcli connection up conname
```

**View connection features**

```
nmcli connection show conname
```


**Desactivate connection**
```
nmcli connection down conname
```

## SSH connection

Installing `sshpass`:

```
sudo apt install sshpass
```



## Links

- [VPN connection on Adaltas cloud](https://www.adaltas.cloud/en/docs/onboarding/vpn/)
- [How to import a OpenVPN .ovpn file with Network Manager or Command Line in Linux](https://www.cyberciti.biz/faq/linux-import-openvpn-ovpn-file-with-networkmanager-commandline/)
- [OpenVPN: Connecting to Access Server with Linux](https://openvpn.net/vpn-server-resources/connecting-to-access-server-with-linux/)
- [How to Set Environment Variables in Linux](https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-set-environment-variables-in-linux/)
- [SSH keys](http://www.ssh.com/academy/ssh/keygen)
