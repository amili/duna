---
slug: "/en/freeipa/client"
date: "2021-05-17"
title: "FreeIPA client installation"
---

# Prerequisites

1. Install FreeIPA server
1. Configure nodes to resolve hostnames with FreeIPA server

## Installation manually

```
yum install freeipa-client -y
```

```
ipa-client-install --mkhomedir --force-ntpd
```

Response of server:

```
Client hostname: worker01.wcluster.local
Realm: WCLUSTER.LOCAL
DNS Domain: wcluster.local
IPA Server: ldap01.wcluster.local
BaseDN: dc=wcluster,dc=local

Continue to configure the system with these values? [no]: yes
Synchronizing time with KDC...
Attempting to sync time using ntpd.  Will timeout after 15 seconds
User authorized to enroll computers: admin
Password for admin@WCLUSTER.LOCAL: 
Successfully retrieved CA cert
    Subject:     CN=Certificate Authority,O=WCLUSTER.LOCAL
    Issuer:      CN=Certificate Authority,O=WCLUSTER.LOCAL
    Valid From:  2021-05-17 08:58:28
    Valid Until: 2041-05-17 08:58:28

Enrolled in IPA realm WCLUSTER.LOCAL
Created /etc/ipa/default.conf
New SSSD config will be created
Configured sudoers in /etc/nsswitch.conf
Configured /etc/sssd/sssd.conf
Configured /etc/krb5.conf for IPA realm WCLUSTER.LOCAL
trying https://ldap01.wcluster.local/ipa/json
[try 1]: Forwarding 'schema' to json server 'https://ldap01.wcluster.local/ipa/json'
trying https://ldap01.wcluster.local/ipa/session/json
[try 1]: Forwarding 'ping' to json server 'https://ldap01.wcluster.local/ipa/session/json'
[try 1]: Forwarding 'ca_is_enabled' to json server 'https://ldap01.wcluster.local/ipa/session/json'
Systemwide CA database updated.
Hostname (worker01.wcluster.local) does not have A/AAAA record.
Incorrect reverse record(s):
10.10.10.16 is pointing to worker01.wcluster. instead of worker01.wcluster.local.
10.10.10.16 is pointing to worker01. instead of worker01.wcluster.local.
Adding SSH public key from /etc/ssh/ssh_host_rsa_key.pub
Adding SSH public key from /etc/ssh/ssh_host_ecdsa_key.pub
Adding SSH public key from /etc/ssh/ssh_host_ed25519_key.pub
[try 1]: Forwarding 'host_mod' to json server 'https://ldap01.wcluster.local/ipa/session/json'
SSSD enabled
Configured /etc/openldap/ldap.conf
NTP enabled
Configured /etc/ssh/ssh_config
Configured /etc/ssh/sshd_config
Configuring wcluster.local as NIS domain.
Client configuration complete.
The ipa-client-install command was successful
```

# Check

On `worker01.wcluster.local` run the command:

```
kinit admin
```

System will propose you to type the password. To check the obtained ticket:

```
klist
```

Example of the server's response:

```
Ticket cache: KEYRING:persistent:0:0
Default principal: admin@WCLUSTER.LOCAL

Valid starting       Expires              Service principal
05/17/2021 12:32:41  05/18/2021 12:32:30  krbtgt/WCLUSTER.LOCAL@WCLUSTER.LOCAL
```

# Links

- [FreeIPA cheatsheet](/en/cheatsheets/freeipa)
- [Install FreeIPA (in Russian)](https://www.dmosk.ru/miniinstruktions.php?mini=freeipa-centos#prepare)

