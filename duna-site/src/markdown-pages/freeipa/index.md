---
slug: "/en/freeipa"
date: "2021-05-03"
title: "FreeIPA. My experiments"
---

1. [Install FreeIPA server](/en/freeipa/install)
1. [Configure nodes to use FreeIPA DNS](/en/freeipa/dns)
1. [Install clients](/en/freeipa/client)
1. [Enable Kerberos with Ambari wizard](/en/freeipa/kerberos)

# Links

- [FreeIPA cheatsheet](/en/cheatsheets/freeipa)
