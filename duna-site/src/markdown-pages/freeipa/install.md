---
slug: "/en/freeipa/install"
date: "2021-05-10"
title: "FreeIPA installation"
---

1. Vagrant machine
1. Ansible playbooks made for group [everybody]:
  - hosts.yml
  - ntp.yml
  - network.yml
  - selinux.yml
  - packages.yml
1. Install FreeIPA server (manually, without Ansible)

```
yum install ipa-server -y
```

```
yum install ipa-server-dns -y
```


```
ipa-server-install
```


```
The log file for this installation can be found in /var/log/ipaserver-install.log
==============================================================================
This program will set up the IPA Server.

This includes:
  * Configure a stand-alone CA (dogtag) for certificate management
  * Configure the Network Time Daemon (ntpd)
  * Create and configure an instance of Directory Server
  * Create and configure a Kerberos Key Distribution Center (KDC)
  * Configure Apache (httpd)
  * Configure the KDC to enable PKINIT

To accept the default shown in brackets, press the Enter key.

WARNING: conflicting time&date synchronization service 'chronyd' will be disabled
in favor of ntpd

```

```
Do you want to configure integrated DNS (BIND)? [no]: yes
Enter the fully qualified domain name of the computer
on which you're setting up server software. Using the form
<hostname>.<domainname>
Example: master.example.com.

Server host name [ldap01.wcluster.local]:
Please confirm the domain name [wcluster.local]: 
Please provide a realm name [WCLUSTER.LOCAL]:
 
Certain directory server operations require an administrative user.
This user is referred to as the Directory Manager and has full access
to the Directory for system management tasks and will be added to the
instance of directory server created for IPA.
The password must be at least 8 characters long.

Directory Manager password: 
Password (confirm): 

The IPA server requires an administrative user, named 'admin'.
This user is a regular system account used for IPA server administration.

IPA admin password: 

```

```
Checking DNS domain wcluster.local., please wait ...
Do you want to configure DNS forwarders? [yes]: yes
Following DNS servers are configured in /etc/resolv.conf: 10.0.2.3
Do you want to configure these servers as DNS forwarders? [yes]: 
All DNS servers from /etc/resolv.conf were added. You can enter additional addresses now:
Enter an IP address for a DNS forwarder, or press Enter to skip: 
Checking DNS forwarders, please wait ...
DNS server 10.0.2.3: answer to query '. SOA' is missing DNSSEC signatures (no RRSIG data)
Please fix forwarder configuration to enable DNSSEC support.
(For BIND 9 add directive "dnssec-enable yes;" to "options {}")
WARNING: DNSSEC validation will be disabled
Do you want to search for missing reverse zones? [yes]: 

The IPA Master Server will be configured with:
Hostname:       ldap01.wcluster.local
IP address(es): 10.10.10.19
Domain name:    wcluster.local
Realm name:     WCLUSTER.LOCAL

BIND DNS server will be configured to serve IPA domain with:
Forwarders:       10.0.2.3
Forward policy:   only
Reverse zone(s):  No reverse zone

Continue to configure the system with these values? [no]:yes

```



Answer from the installed server:

```
Setup complete

Next steps:
	1. You must make sure these network ports are open:
		TCP Ports:
		  * 80, 443: HTTP/HTTPS
		  * 389, 636: LDAP/LDAPS
		  * 88, 464: kerberos
		  * 53: bind
		UDP Ports:
		  * 88, 464: kerberos
		  * 53: bind
		  * 123: ntp

	2. You can now obtain a kerberos ticket using the command: 'kinit admin'
	   This ticket will allow you to use the IPA tools (e.g., ipa user-add)
	   and the web user interface.

Be sure to back up the CA certificates stored in /root/cacert.p12
These files are required to create replicas. The password for these
files is the Directory Manager password

```

## Check installation

On the server run a command to obtain the ticket for user `admin`:

```
# kinit admin
Password for admin@WCLUSTER.LOCAL: 
# klist
Ticket cache: KEYRING:persistent:0:0
Default principal: admin@WCLUSTER.LOCAL

Valid starting       Expires              Service principal
05/10/2021 18:16:57  05/11/2021 18:16:44  krbtgt/WCLUSTER.LOCAL@WCLUSTER.LOCAL
```

Web-interface `http://ldap01.wcluster.local` for the moment browser will prevent about unsecure connection.


# Next steps

- [Configure nodes to use FreeIPA DNS](/en/freeipa/dns)


# Links

- [FreeIPA cheatsheet](/en/cheatsheets/freeipa)
- [Install FreeIPA (in Russian)](https://www.dmosk.ru/miniinstruktions.php?mini=freeipa-centos#prepare)

