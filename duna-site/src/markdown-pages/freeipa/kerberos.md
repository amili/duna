---
slug: "/en/freeipa/kerberos"
date: "2021-05-10"
title: "Configuring authentication with kerberos"
---

# Prerequisites

- Installed FreeIPA
- Installed Ambari server with Java Cryptography Extensions (JCE) on all nodes

# Creating an IPA account for use with Ambari

```
kinit admin
```

Create `hadoop` user

```
ipa user-add hadoopadmin --first=Hadoop --last=Admin --password
```

Enter a temporal password for `hadoopadmin` and repeat the it.


Server's response example:

```
------------------------
Added user "hadoopadmin"
------------------------
  User login: hadoopadmin
  First name: Hadoop
  Last name: Admin
  Full name: Hadoop Admin
  Display name: Hadoop Admin
  Initials: HA
  Home directory: /home/hadoopadmin
  GECOS: Hadoop Admin
  Login shell: /bin/sh
  Principal name: hadoopadmin@WCLUSTER.LOCAL
  Principal alias: hadoopadmin@WCLUSTER.LOCAL
  User password expiration: 20210517160611Z
  Email address: hadoopadmin@wcluster.local
  UID: 1173600001
  GID: 1173600001
  Password: True
  Member of groups: ipausers
  Kerberos keys available: True
```

```
# create a role and and give it privilege to manage users and services
ipa role-add hadoopadminrole
ipa role-add-privilege hadoopadminrole --privileges="User Administrators" 
ipa role-add-privilege hadoopadminrole --privileges="Service Administrators"

# add the hadoopadmin user to the role
ipa role-add-member hadoopadminrole --users=hadoopadmin

# login once, or kinit, to reset the initial temporary password for the hadoopadmin account
kinit hadoopadmin
```
Service FreeIPA will ask you the temporal password and propose to change it.

```
Password for hadoopadmin@WCLUSTER.LOCAL: 
Password expired.  You must change it now.
Enter new password: 
Enter it again: 
```

> Do not install an Ambari Agent on the IPA host!

# Configure krb5.conf Credential Cache

HDP does not support the in-memory keyring storage of the Kerberos credential cache. Edit the /etc/krb5.conf file and change:

`default_ccache_name = KEYRING:persistent:%{uid}`
to

`default_ccache_name = FILE:/tmp/krb5cc_%{uid}`

> I did this only on `ldap01.wcluster.local`

# Enable Kerberos with Ambari wizard

Choose `Cluster Admin` -> `Kerberos` and press `Enable`

```
YARN log and local dir will be deleted and ResourceManager state will be formatted as part of Enabling/Disabling Kerberos.
```

## Short description

> What I changed at each step

1. Get started 

What type of KDS do you plan on using? **Existing IPA**


```
Existing IPA:
Following prerequisites needs to be checked to progress ahead in the wizard.

All cluster hosts are joined to the IPA domain and hosts are registered in DNS
A password policy is in place that sets no expiry for created principals
If you do not plan on using Ambari to manage the krb5.conf, ensure the following is set in each krb5.conf file in your cluster: default_ccache_name = /tmp/krb5cc_%{uid}
The Java Cryptography Extensions (JCE) have been setup on the Ambari Server host and all hosts in the cluster.
```

2. Configure Kerberos

**KDS**

KDS hosts: **10.10.10.19**
Realm name: **WCLUSTER.LOCAL**
Domains: **wcluster.local**

Kadmin host: **ldap01.wcluster.local**
Admin principal: **admin**
Admin password: **[password for admin freeipa user]**

3. Install and test Kerberos clients - just wait

4. Configure Identities - by default

5. Confirm Configuration - by default

6. Stop services - wait

7. Kerberize cluster - wait

8. Start and test services

> I re-launched step 8 several times. Maybe the reason is memory issues, but the failed service started from next try.

## Long description

> Many things are repeated

1. Get started

What type of KDS do you plan on using? **Existing IPA**


```
Existing IPA:
Following prerequisites needs to be checked to progress ahead in the wizard.

All cluster hosts are joined to the IPA domain and hosts are registered in DNS
A password policy is in place that sets no expiry for created principals
If you do not plan on using Ambari to manage the krb5.conf, ensure the following is set in each krb5.conf file in your cluster: default_ccache_name = /tmp/krb5cc_%{uid}
The Java Cryptography Extensions (JCE) have been setup on the Ambari Server host and all hosts in the cluster.
```


2. Configure Kerberos

**KDS**

KDS hosts: **10.10.10.19**
Realm name: **WCLUSTER.LOCAL**
Domains: **wcluster.local**

Kadmin host: **ldap01.wcluster.local**
Admin principal: **admin**
Admin password: **[password for admin freeipa user]**


All theese I left by default

```
Advanced kerberos-env
Install OS-specific Kerberos client package(s)
 
Executable Search Paths
/usr/bin, /usr/kerberos/bin, /usr/sbin, /usr/lib/mit/bin, /usr/lib/mit/sbin
Encryption Types
aes des3-cbc-sha1 rc4 des-cbc-md5
Test Kerberos Principal
${cluster_name|toLower()}-${short_date}
 
Enable case insensitive username rules
 
Manage Hadoop auth_to_local rules
 
Create Ambari Principal & Keytab
 
IPA User Group
 
Master KDC host
 
Pre-configure services
DEFAULT
 
Service Check Retry Count
9
 
Service Check Retry Period
15
 
```

And this also

```
Advanced krb5-conf
Manage Kerberos client krb5.conf
 
krb5-conf directory path
/etc
 
krb5-conf template
```

3. Install and test Kerberos clients

Just wait

4. Configure Identities

Everything is by default

Global
```
Keytab Dir
/etc/security/keytabs
 
Realm
WCLUSTER.LOCAL
 
Additional Realms
(Optional)
 
Principal Suffix
-${cluster_name|toLower()}
 
Spnego Keytab
${keytab_dir}/spnego.service.keytab
 
Spnego Principal
HTTP/_HOST@${realm}
 
Ambari Principals
Smoke user keytab
${keytab_dir}/smokeuser.headless.keytab
 
Smoke user principal
${cluster-env/smokeuser}${principal_suffix}@${realm}
 
Superset user keytab
${keytab_dir}/superset.headless.keytab
 
Superset user principal
${superset-env/superset_user}${principal_suffix}@${realm}
 
Accumulo user principal
${accumulo-env/accumulo_user}${principal_suffix}@${realm}
 
Accumulo user keytab
${keytab_dir}/accumulo.headless.keytab
 
Ambari Keytab
${keytab_dir}/ambari.server.keytab
 
Ambari Principal Name
ambari-server${principal_suffix}@${realm}
 
druid.escalator.internalClientKeytab
${keytab_dir}/druid.headless.keytab
 
druid.escalator.internalClientPrincipal
${druid-env/druid_user}${principal_suffix}@${realm}
 
Druid user keytab
${keytab_dir}/druid.headless.keytab
 
Druid user principal
${druid-env/druid_user}${principal_suffix}@${realm}
 
HBase user principal
${hbase-env/hbase_user}${principal_suffix}@${realm}
 
HBase user keytab
${keytab_dir}/hbase.headless.keytab
 
HDFS user principal
${hadoop-env/hdfs_user}${principal_suffix}@${realm}
 
HDFS user keytab
${keytab_dir}/hdfs.headless.keytab
 
Spark2user Keytab
${keytab_dir}/spark.headless.keytab
 
Spark2user Principal
${spark2-env/spark_user}${principal_suffix}@${realm}
 
Storm user keytab
${keytab_dir}/storm.headless.keytab
 
Storm user principal
${storm-env/storm_user}${principal_suffix}@${realm}
 
trace.token.property.keytab
${keytab_dir}/accumulo-tracer.headless.keytab
 
trace.user
tracer${principal_suffix}@${realm}
 
yarn_ats_principal_name
${yarn-env/yarn_ats_user}${principal_suffix}@${realm}
 
yarn_ats_user_keytab
${keytab_dir}/yarn-ats.hbase-client.headless.keytab
 
zeppelin.server.kerberos.keytab
${keytab_dir}/zeppelin.server.kerberos.keytab
 
zeppelin.server.kerberos.principal
${zeppelin-env/zeppelin_user}${principal_suffix}@${realm}
```

5. Confirm Configuration

It was by default

```
Executable path: /usr/bin, /usr/kerberos/bin, /usr/sbin, /usr/lib/mit/bin, /usr/lib/mit/sbin
KDC Hosts: 10.10.10.19
KDC Type: Existing IPA
Realm Name: WCLUSTER.LOCAL
```
> Using the Download CSV button, you can download a csv file which contains a list of the principals and keytabs that will automatically be created by Ambari.


# Links

- [FreeIPA cheatsheet](/en/cheatsheets/freeipa)
- [Install FreeIPA](/en/freeipa/install)
- [Configuring Authentication with Kerberos](https://docs.cloudera.com/HDPDocuments/HDP3/HDP-3.0.0/authentication-with-kerberos/content/enabling_kerberos_authentication_using_ambari.html)
- [Configuring Authentication with Kerberos: Use an Existing IPA](https://docs.cloudera.com/HDPDocuments/HDP3/HDP-3.0.0/authentication-with-kerberos/content/kerberos_optional_use_an_existing_ipa.html)
